﻿import java.util.Scanner;
import java.io.*;

class Fahrkartenautomat {

	public static void main(String[] args) {

		// byte ticket //Ich habe den Datentypen byte gewählt weil es der kleinste
		// Datentyp ist der verwendbar ist.
		// Ich habe ticket als Variabele gewählt weil es sehr verständlich ist.
		// double tpreis // Ich habe double benutzt weil die Variabele tpreis auch
		// Gleitkommastellen mit berücksichtigen soll.
		// Ich habe tpreis als Variabele gewählt weil es sehr verständlich ist

		double zuZahldenderBetrag = fahrkartenbestellungErfassen();
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahldenderBetrag);
		rueckgeldAusgeben(zuZahldenderBetrag, eingezahlterGesamtbetrag);
		fahrkartenAusgeben();
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

		// Als Erklärung falls Sie sich fragen was ich hier gemacht habe,
		// ich wollte eine reset-Funktion einbauen um nicht immer das Programm
		// neustarten zu müssen.
	}

	public static double fahrkartenbestellungErfassen() {
		double tpreis;
		byte ticket;
		double zuZahlenderBetrag = 0;
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Ticketpreis: ");
		tpreis = tastatur.nextDouble();
		if (tpreis < 0) {
			System.out.println("Das ist keine zulässige Anzahl an Tickets!");
			System.exit(0);
		} else {
			System.out.print("Anzahl der Tickets: ");
			ticket = tastatur.nextByte();
			if (ticket > 10 || ticket < 0) {
				System.out.println("Das ist keine zulässige Anzahl an Tickets!");
				System.exit(0);
			} else {
				zuZahlenderBetrag = ticket * tpreis;
				System.out.print("Zu zahlender Betrag (EURO): ");
				System.out.printf("%.2f\n", zuZahlenderBetrag);
			}
		}
		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze = 0;
		eingezahlterGesamtbetrag = 0.00;
		Scanner eingabe = new Scanner(System.in);
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.println("Noch zu zahlen: ");
			System.out.printf("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.println(" Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

			eingeworfeneMünze = eingabe.nextDouble();

			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		return eingezahlterGesamtbetrag;

	}

	public static int warte() {
		int w = 250;
		return w;
	}

	public static void fahrkartenAusgeben() {
		int w = warte();
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {

				Thread.sleep(w);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
		/*
		 * public static void muenzeraus() {
		 * 
		 * }
		 */
	}

	private static double rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double rückgabebetrag;

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
		return rückgabebetrag;

	}
}