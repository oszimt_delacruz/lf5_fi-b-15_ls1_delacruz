
public class Aufgabe_2 {
	public static void aufgabe2() {	
		System.out.println("Aufgabe 2:");
		System.out.println(" ");
		System.out.printf("%-4s=%-19s=%5d\n" , "0!", " ", 1);
		System.out.printf("%-4s=%-19s=%5d\n" , "1!", " 1 ", 1);
		System.out.printf("%-4s=%-19s=%5d\n" , "2!", " 1 * 2 ", 2);
		System.out.printf("%-4s=%-19s=%5d\n" , "3!", " 1 * 2 * 3 ", 6);
		System.out.printf("%-4s=%-19s=%5d\n" , "4!", " 1 * 2 * 3 * 4 ", 24);
		System.out.printf("%-4s=%-19s=%5d\n" , "4!", " 1 * 2 * 3 * 4 * 5 ", 120);
	}

}
