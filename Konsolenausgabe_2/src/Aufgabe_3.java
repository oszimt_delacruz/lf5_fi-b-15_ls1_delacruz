
public class Aufgabe_3 {
	public static void main  (String[] args) {
	System.out.println("Aufgabe 3:"); 
	//Zur beschreibung der folgenden Abbildung, wird "Aufgabe 3" in der Konsole ausgegeben. 
	System.out.println(" ");         
	// F�r die Optik 
	double a = -28.8889; 
	// Zur verk�rzung der Print-Befehle deklarieren ich hier die Werte als Variabelen.
	double b = -23.333;
	double c = -17.7778;
	double d = -6.6667;
	double e = -1.1111;   
	System.out.printf("%-12s|%10s\n" , "Fahrenheit", "Celsius");
	//Ich gebe mit hilfe von zwei Argumenten, die zwei Seiten der Tabelle an.
	System.out.printf("-----------------------\n");
	// Einf�gen der Trennzeile.
	System.out.printf("%-12s|%10.2f\n", "-20", a);
	//Die Print-Befehle werden, mit dem ersten Argument linksb�dig mit mindestens 12 Stellen ausgef�hrt
	// und rechtsb�ndig mit 10 Stellen f�r das zweite Argument was die vorher deklariete Variable ist.
	System.out.printf("%-12s|%10.2f\n", "-10", b);
	System.out.printf("%-12s|%10.2f\n", "0", c);
	System.out.printf("%-12s|%10.2f\n", "10", d);
	System.out.printf("%-12s|%10.2f\n", "20", e);
	}

}
