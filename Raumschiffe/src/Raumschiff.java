import java.util.ArrayList;

public class Raumschiff {

	private int torpedoanzahl;
	private int energieversorgungprozent;
	private int schildeprozent;
	private int huelleprozent;
	private int lebenserhaltungssysteme;
	private int androidenanzahl;
	private String name;
	private ArrayList<Ladung> ladungsverzeichnis;
	private static ArrayList<String> broadcast;
	private static ArrayList<String> logbuch;
	
	public Raumschiff() {
		ladungsverzeichnis = new ArrayList<Ladung>();
		broadcast = new ArrayList<String>();
		logbuch = new ArrayList<String>();
	}

	public Raumschiff(int torpedoanzahl, int energieversorgungprozent, int schildeprozent, int huelleprozent,
			int lebenserhaltungssysteme, int androidenanzahl, String name) {
		ladungsverzeichnis = new ArrayList<Ladung>();
		broadcast = new ArrayList<String>();
		
		this.name = name;
		this.torpedoanzahl= torpedoanzahl;
		this.energieversorgungprozent= energieversorgungprozent;
		this.schildeprozent = schildeprozent;
		this.huelleprozent= huelleprozent;
		this.lebenserhaltungssysteme= lebenserhaltungssysteme;
		this.androidenanzahl= androidenanzahl;
	}

	public int getTorpedoanzahl() {
		return torpedoanzahl;
	}

	public void setTorpedoanzahl(int torpedoanzahl) {
		this.torpedoanzahl = torpedoanzahl;
	}

	public int getEnergieversorgungprozent() {
		return energieversorgungprozent;
	}

	public void setEnergieversorgungprozent(int energieversorgungprozent) {
		this.energieversorgungprozent = energieversorgungprozent;
	}

	public int getSchildeprozent() {
		return schildeprozent;
	}

	public void setSchildeprozent(int schildeprozent) {
		this.schildeprozent = schildeprozent;
	}

	public int getHuelleprozent() {
		return huelleprozent;
	}

	public void setHuelleprozent(int huelleprozent) {
		this.huelleprozent = huelleprozent;
	}

	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}

	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}

	public int getAndroidenanzahl() {
		return androidenanzahl;
	}

	public void setAndroidenanzahl(int androidenanzahl) {
		this.androidenanzahl = androidenanzahl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addladung(Ladung neueladung) {
		ladungsverzeichnis.add(neueladung);
	}

	public void torpedosschiessen(Raumschiff r) {

	}

	public void kanonenschiessen(Raumschiff r) {

	}

	private void treffer(Raumschiff r) {

	}

	public void nachricht(String message) {

	}

	public static ArrayList<String> logbuchzur�ckgeben() {
		return logbuch;
	}

	public void torpedosladen(int torpedosanzahl) {

	}

	public void reperaturen(boolean schutzschilde, boolean energieversorung, boolean schiffhuelle, int anzahldroiden) {

	}
	
	public void zustandRaumschiff() {
		System.out.print("Name: "+name+"\n Anzahl der Torpedos: "+ "");
	}

}
