
public class Raumschiffmain {

	public static void main(String[] args) {
		
		// Raumschiffe erzeugt und Ladung hizugefügt.
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,2,"IKS heg'ta");
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'Var");
		
		vulkanier.addladung(new Ladung("Forschungssonde", 35));
		vulkanier.addladung(new Ladung("Photonentorpedo",3));
	
		klingonen.addladung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addladung(new Ladung("Bat'leth Klingonen Schwert",200));
		
		romulaner.addladung(new Ladung("Borg-Schrott", 5));
		romulaner.addladung(new Ladung("Rote Materie",2));
		romulaner.addladung(new Ladung("Plasma-Waffe", 50));
		
		// Zustand des Schiffes ausgeben
		System.out.print(klingonen.getName()+klingonen.getName()+klingonen.getName()+klingonen.getName());
	}
		

}
