
public class Artikelverwaltung {

	public static void main(String[] args) {
		Artikel art1 = new Artikel();
		Artikel art2 = new Artikel();
		Artikel art3 = new Artikel();
		
		art1.setName("Wasserflasche");
		art2.setName("Bierkasten");
		art3.setName("Vodkaflasche");
		
		art1.setartnr(5);
		art2.setartnr(6);
		art3.setartnr(42);
		
		art1.setminbestand(5);
		art2.setminbestand(50);
		art3.setminbestand(25);
		
		art1.setlagerbestand(9);
		art2.setlagerbestand(70);
		art3.setlagerbestand(30);
		
		art1.seteinkauf(13.25);
		art2.seteinkauf(17.69);
		art3.seteinkauf(23.98);
		
		art1.setverkauf(19.45);
		art2.setverkauf(24.89);
		art3.setverkauf(29.99);
		
		System.out.println("Name: " + art1.getName());
		System.out.println("Artikelnummer: " + art1.getartnr());
		System.out.println("Mindestbestand: " + art1.getminbestand() + " St�ck m�ssen mind. vorhanden sein!");
		System.out.println("Lagerbestand: " + art1.getlagerbestand() + " St�ck sind vorhanden.");
		System.out.println("Einkaufspreis: " + art1.geteinkauf() + " Euro");
		System.out.println("Verkaufsspreis: " + art1.getverkauf() + " Euro");
		
		System.out.println("");
		
		System.out.println("Name: " + art2.getName());
		System.out.println("Artikelnummer: " + art2.getartnr());
		System.out.println("Mindestbestand: " + art2.getminbestand() + " St�ck m�ssen mind. vorhanden sein!");
		System.out.println("Lagerbestand: " + art2.getlagerbestand() + " St�ck sind vorhanden.");
		System.out.println("Einkaufspreis: " + art2.geteinkauf() + " Euro");
		System.out.println("Verkaufsspreis: " + art2.getverkauf() + " Euro");
		
		System.out.println("");
		
		System.out.println("Name: " + art3.getName());
		System.out.println("Artikelnummer: " + art3.getartnr());
		System.out.println("Mindestbestand: " + art3.getminbestand() + " St�ck m�ssen mind. vorhanden sein!");
		System.out.println("Lagerbestand: " + art3.getlagerbestand() + " St�ck sind vorhanden.");
		System.out.println("Einkaufspreis: " + art3.geteinkauf() + " Euro");
		System.out.println("Verkaufsspreis: " + art3.getverkauf() + " Euro");
	}
}
