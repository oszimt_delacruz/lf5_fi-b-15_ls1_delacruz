
public class Artikel {
	private String name;
	private int artnr;
	private int minbestand;
	private int lagerbestand;
	private double einkauf;
	private double verkauf;
	
	public Artikel() 
	{
		
	} 
	
	public void setName(String name)
	 {
	 this.name = name;
	 }
	 public String getName()
	 {
	 return this.name;
	 }
	 
	 public void setartnr(int artnr)
	 {
	 this.artnr= artnr;
	 }
	 public int getartnr()
	 {
	 return this.artnr;
	 }
	 
	 public void setminbestand(int minbestand)
	 {
	 this.minbestand= minbestand;
	 }
	 public int getminbestand()
	 {
	 return this.minbestand;
	 }
	 
	 public void setlagerbestand(int lagerbestand)
	 {
	 this.lagerbestand= lagerbestand;
	 }
	 public int getlagerbestand()
	 {
	 return this.lagerbestand;
	 }
	 
	 public void seteinkauf(double einkauf)
	 {
	 this.einkauf= einkauf;
	 }
	 public double geteinkauf()
	 {
	 return this.einkauf;
	 }
	 
	 public void setverkauf(double verkauf)
	 {
	 this.verkauf= verkauf;
	 }
	 public double getverkauf()
	 {
	 return this.verkauf;
	 }
	 
	 public void bestellen()
		{		

		}
	 public double gewinnmarge() 
	 {
		 double gewinn = verkauf-einkauf;
		 
		 return gewinn;
	 } 
	 public boolean checklager() 
	 { boolean da ;
		 if (minbestand*0.80<lagerbestand) {
		 da = true;
		 } else {
		da = false;
		 }
		 return da;
	 }
}
